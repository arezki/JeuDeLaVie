package Jeu;
import java.io.*;


import java.util.Properties;
import java.util.Scanner;

/**
* <b> GenererHtml est la classe qui retoune le type d’évolution de tous les
* jeux contenus dans le dossier passé en paramètre et affiche les résultats sous la forme d’un fichier
* html.</b>
* <p>
* Un Générateur HTML est caractérisée par :
* <ul>
* <li>Une chaine de caractère (contenu du fichier HTML à générer).</li>
* <li>type du monde du jeu de la vie.</li>
* </ul>
* </p>
* @see Jeu
* @see Monde
* @see MondeCirculaire
* @see MondeFrontiere
*
*@author nassim
*/

public class GenererHtml {
	Jeu jeux ;
	EtudesAsymptote et;
	Monde uni;
	private static StringBuffer deplacementJeux= new StringBuffer("<th>Déplacement</th>");
	private static StringBuffer periodeJeux= new StringBuffer("<th>Période</th>");
	private static StringBuffer tailleQueue= new StringBuffer("<th>Taille</th>");
	private static StringBuffer typeJeux = new StringBuffer("<th>Type</th>");
	private static StringBuffer nomFichier=new StringBuffer("<th>Nom fichier</th>");
	public GenererHtml(String dossier, int max) throws IOException {
		int monde;
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("Monde Infinis Taper 1");
			System.out.println("*****************");
			System.out.println("Monde Circulaire Taper 2");
			System.out.println("*****************");
			System.out.println("Monde Frontière Taper 3");
			monde = sc.nextInt();
		} while (monde != 1 && monde != 2 && monde != 3);
		File f = new File(dossier);
		PrintWriter fichierhtml=null;
		try {
			fichierhtml = new PrintWriter(new FileWriter("index.html",true));
			fichierhtml.write(debut());
			if (!f.isDirectory()) {
				throw new ArgumentException("Erreur Arguments");
			}
			String nom_dossier = f.getName();
			for(File file : f.listFiles()){
				jeux = new Jeu();
				String nom_du_fichier= file.getName();
				uni = new Monde(nom_dossier+"/"+nom_du_fichier,monde);
				et=new EtudesAsymptote(uni,nom_dossier+"/"+nom_du_fichier);
				jeux.EvolutionPourHtml(max, uni, et);
				ecrireDeplacementJeux(jeux.deplacement);
				ecrireNomFichier(nom_du_fichier);
				ecrirePeriodeJeux(jeux.periode);
				ecrireTailleQueue(jeux.taille);
				ecrireTypeJeux(jeux.type);
			}
			fichierhtml.write("           <tr>"+nomFichier+"</tr>\n");
			fichierhtml.write("           <tr>"+typeJeux+"</tr>\n");
			fichierhtml.write("           <tr>"+tailleQueue+"</tr>\n");
			fichierhtml.write("           <tr>"+periodeJeux+"</tr>\n");
			fichierhtml.write("           <tr>"+deplacementJeux+"</tr>\n");
			fichierhtml.write(end());

		}catch(Exception e){
			e.printStackTrace();
		}finally {
			fichierhtml.close();
		}

	}

	
	
	/*
	 * methode qui permer d Ouvrir la page html
	 * 
	 * 
	 */
	public void openFile(String s) {
		Properties sys = System.getProperties();
		String os = sys.getProperty("os.name");
		Runtime r = Runtime.getRuntime();
		try {
			if (os.endsWith("OS") || os.endsWith("XP") || os.endsWith("Vista")) {
				r.exec("cmd /c start http://fr.wikipedia.org/wiki/Jeu_de_la_vie");
			} else {
				r.exec("firefox ./" + s);
			}
		} catch (IOException ex) {
			ex.printStackTrace();

		}
}


	public String debut(){
		String str="<!DOCTYPE html>\n <html>\n  <head>\n   <meta charset=\"UTF-8\"/>" +
				"\n  </head>\n  <body>\n   <p> Les Défferentes evolutions des jeux du dossier courant</p>\n" +
				"    <table>\n" +
				"    <caption>Jeux de la vie</caption>\n";
		return str;
	}
    public String end(){
		String str="    </table>\n  </body>\n</html>";
		return str;
	}
     public void ecrireNomFichier(String nom){
     	nomFichier.append("<th>"+nom+"</th>");
	 }
	public void ecrirePeriodeJeux(int periode){
		periodeJeux.append("<td>"+periode+"</td>");
	}
	public void ecrireTypeJeux(String type){
		typeJeux.append("<td>"+type+"</td>");
	}
	public void ecrireTailleQueue(int taille){
		tailleQueue.append("<td>"+taille+"</td>");
	}
	public void ecrireDeplacementJeux(String deplacement){
		deplacementJeux.append("<td>"+deplacement+"</td>");
	}
}
