
package Jeu;
/**
 * <b>
 * Classe MondeFrontiere représente un monde avec frontieres (échiquier fini)
 * Cette classe hérite de la classe Monde et a donc les memes attributs 
 * la méthode checkCoord est redéfinie  . <b>
 * @author arezki
 */
public class MondeFrontiere extends Monde {

	public MondeFrontiere() {
		super();
	}

	
	public MondeFrontiere(String file, int id_monde) {
		super(file,id_monde);
	}


	/**
	 * Retourne la coordonnée construite à partir des paramètres. Si la coordonnée dépasse un bord,
	 * on met le booléen valide à false.
	 * 
	 * @param l
	 *            la ligne
	 * @param c
	 *            la colonne
	 *            
	 * @return La ligne et la colonne sous forme de coordonnées
	 */
	public Coordonnees checkCoord(int l, int c) {
		Coordonnees s = new Coordonnees(l, c);
		if (l < 0 || c < 0 || l >= this.getNbligne()-1
				|| c >= this.getNbcolonne()-1) {
			s.x = l;
			s.y = c;
			s.valide = false;
		}
		return s;
	}
}